package com.explore.demo.controller;


import com.explore.demo.common.ResultResponse;
import com.explore.demo.pojo.TblUser;
import com.explore.demo.service.TblUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author scy
 * @since 2022-05-31
 */
@RestController
@RequestMapping("/tbl_user")
@Api(tags = "user-controller")
public class TblUserController {

    @Resource
    private TblUserService tblUserService;

    @GetMapping("/login")
    @ApiOperation(value = "用户登录")
    public ResultResponse<Object> login(String phone, String password){
        return tblUserService.login(phone, password);
    }

    @PostMapping("/register")
    @ApiOperation(value = "用户注册")
    public ResultResponse<Object>  register(String phone, String password){
        return tblUserService.register(phone, password);
    }

    @PostMapping("/update")
    @ApiOperation(value = "用户更新信息")
    public ResultResponse<Object>  updateUserInfo(TblUser user){
        return tblUserService.updateUserInfo(user);
    }

    @GetMapping("/query_page")
    @ApiOperation(value = "用户分页查询")
    public ResultResponse<Object>  queryUserInfo(String username,Integer offset, Integer total){
        return tblUserService.queryUserInfo(username, offset, total);
    }
}

