package com.explore.demo.service;

import com.explore.demo.pojo.TblOrder;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author scy
 * @since 2022-05-31
 */
public interface TblOrderService extends IService<TblOrder> {

}
