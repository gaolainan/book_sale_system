package com.explore.demo.service.impl;

import com.explore.demo.pojo.TblOrder;
import com.explore.demo.mapper.TblOrderMapper;
import com.explore.demo.service.TblOrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author scy
 * @since 2022-05-31
 */
@Service
public class TblOrderServiceImpl extends ServiceImpl<TblOrderMapper, TblOrder> implements TblOrderService {

}
