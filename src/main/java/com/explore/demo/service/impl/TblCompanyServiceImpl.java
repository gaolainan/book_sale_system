package com.explore.demo.service.impl;

import com.explore.demo.pojo.TblCompany;
import com.explore.demo.mapper.TblCompanyMapper;
import com.explore.demo.service.TblCompanyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author scy
 * @since 2022-05-31
 */
@Service
public class TblCompanyServiceImpl extends ServiceImpl<TblCompanyMapper, TblCompany> implements TblCompanyService {

}
