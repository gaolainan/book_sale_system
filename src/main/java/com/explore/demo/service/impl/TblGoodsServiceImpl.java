package com.explore.demo.service.impl;

import com.explore.demo.pojo.TblGoods;
import com.explore.demo.mapper.TblGoodsMapper;
import com.explore.demo.service.TblGoodService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author scy
 * @since 2022-05-31
 */
@Service
public class TblGoodsServiceImpl extends ServiceImpl<TblGoodsMapper, TblGoods> implements TblGoodService {

}
