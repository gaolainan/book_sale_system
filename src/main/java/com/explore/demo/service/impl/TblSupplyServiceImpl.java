package com.explore.demo.service.impl;

import com.explore.demo.pojo.TblSupply;
import com.explore.demo.mapper.TblSupplyMapper;
import com.explore.demo.service.TblSupplyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author scy
 * @since 2022-05-31
 */
@Service
public class TblSupplyServiceImpl extends ServiceImpl<TblSupplyMapper, TblSupply> implements TblSupplyService {

}
