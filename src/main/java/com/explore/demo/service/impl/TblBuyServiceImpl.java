package com.explore.demo.service.impl;

import com.explore.demo.pojo.TblBuy;
import com.explore.demo.mapper.TblBuyMapper;
import com.explore.demo.service.TblBuyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author scy
 * @since 2022-05-31
 */
@Service
public class TblBuyServiceImpl extends ServiceImpl<TblBuyMapper, TblBuy> implements TblBuyService {

}
