package com.explore.demo.service.impl;

import com.explore.demo.pojo.TblStock;
import com.explore.demo.mapper.TblStockMapper;
import com.explore.demo.service.TblStockService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author scy
 * @since 2022-05-31
 */
@Service
public class TblStockServiceImpl extends ServiceImpl<TblStockMapper, TblStock> implements TblStockService {

}
