package com.explore.demo.service.impl;

import com.explore.demo.pojo.TblType;
import com.explore.demo.mapper.TblTypeMapper;
import com.explore.demo.service.TblTypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author scy
 * @since 2022-05-31
 */
@Service
public class TblTypeServiceImpl extends ServiceImpl<TblTypeMapper, TblType> implements TblTypeService {

}
