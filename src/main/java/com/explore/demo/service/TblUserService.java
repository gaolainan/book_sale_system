package com.explore.demo.service;

import com.explore.demo.common.ResultResponse;
import com.explore.demo.pojo.TblUser;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.web.servlet.function.ServerResponse;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author scy
 * @since 2022-05-31
 */
public interface TblUserService extends IService<TblUser> {
    /**
     * 用户登录的接口
     * @param phone
     * @param password
     * @return
     */
    public ResultResponse<Object> login(String phone, String password);


    /**
     * 注册用户
     * @param username
     * @param password
     * @return
     */
    public ResultResponse<Object>  register(String username, String password);

    /**
     * 更新用户信息
     * @param user
     * @return
     */
    public ResultResponse<Object>  updateUserInfo(TblUser user);

    /**
     * 根据用户名称模糊查询
     * @param username
     * @param offset
     * @param total
     * @return
     */
    public ResultResponse<Object>  queryUserInfo(String username,Integer offset, Integer total);

}
