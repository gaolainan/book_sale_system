package com.explore.demo.service;

import com.explore.demo.pojo.TblStock;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author scy
 * @since 2022-05-31
 */
public interface TblStockService extends IService<TblStock> {

}
