package com.explore.demo.common;

/**
 * @author scy
 */
public enum ResultCode {

    SUCCESS(200, "SUCCESS"),
    ERROR(100, "ERROR"),
    // 跟空值有关的
    CHECK_NULL_INFO(4000,"查询为空"),
    NEED_LOGIN(202,"NEED_LOGIN"),
    ILLEGAL_ARGUMENT(300, "ILLEGAL_ARGUMENT"),

    // 关于用户表有关的错误 3000
    ACCOUNT_IS_NULL(3000, "账号为空"),
    PASSWORD_IS_NULL(3001,"密码为空"),
    USER_CREATE_FAIL(3002,"创建用户失败"),
    USER_ALREADY_EXISTS(3003,"用户已存在"),
    PASSWORD_IS_ERROR(3004,"密码错误"),
    LOG_OUT_FAIL(3005,"注销失败"),
    USER_NOT_LOGIN(3006,"用户未登录"),
    USER_IS_NULL(3007,"传进来的用户为空"),
    UPDATE_ERROR(3008,"更新错误"),
    UPDATE_FAIL(3009,"更新失败"),
    VERIFY_CODE_IS_NULL(3010,"传入的验证码为空"),

    // 与类别相关的 2000
    CATEGORY_IS_NULL(2000,"传入的category为空"),
    CATEGORY_INSERT_FAIL(2001,"插入category失败"),
    CATEGORY_NAME_IS_NULL(2002,"传入的categoryName为空"),
    CATEGORY_DELETE_FAIL(2003,"删除category失败"),

    // 与订单相关的 5000
    COMPANY_IS_NULL(5000,"传入的企业为空"),
    COMPANY_INSERT_FAIL(5001,"创建企业失败"),
    COMPANY_NAME_IS_NULL(5002,"传入的企业的名称名称为空"),
    COMPANY_DELETE_FAIL(5003,"删除企业失败"),

    // 与订单相关的 6000
    DEPARTMENT_IS_NULL(6000,"传入的部门为空"),
    DEPARTMENT_INSERT_FAIL(6001,"创建部门失败"),
    DEPARTMENT_NAME_IS_NULL(6002,"传入的部门名称为空"),
    DEPARTMENT_DELETE_FAIL(6003,"删除部门失败"),

    // 与评价相关的 7000
    COMMENT_IS_NULL(7000,"传入的评论为空"),
    COMMENT_INSERT_FAIL(7001,"创建评论失败"),
    COMMENT_ID_IS_NULL(7002,"传入的评论id为空"),
    COMMENT_DELETE_FAIL(7003,"删除评论失败"),

    // 与评价相关的 8000
    HUMAN_IS_NULL(8000,"传入的hr信息为空"),
    HUMAN_INSERT_FAIL(8001,"创建hr失败"),
    HUMAN_NAME_IS_NULL(8002,"hr的名称为空"),
    HUMAN_ID_IS_NULL(8003,"hr的id为空"),
    HUMAN_DELETE_FAIL(8004,"删除hr失败"),

    // 与评价相关的 9000
    RECRUITMENT_IS_NULL(9000,"传入的招聘消息为空"),
    RECRUITMENT_INSERT_FAIL(9001,"创建招聘消息失败"),
    RECRUITMENT_TITLE_IS_NULL(9002,"招聘消息的title为空"),
    RECRUITMENT_TO_ID_IS_NULL(8003,"离线消息的接收方的id为空"),
    RECRUITMENT_DELETE_FAIL(8004,"删除招聘消息失败");

    private int code;
    private String description;

    ResultCode(int code, String description) {
        this.code = code;
        this.description = description;
    }

    public int getCode() {
        return this.code;
    }

    public String getDescription() {
        return this.description;
    }
}
