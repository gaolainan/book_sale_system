package com.explore.demo.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * @author lscy
 */
@Data
@AllArgsConstructor
@Getter
@Setter
@Accessors(chain = true)
public class ResultResponse<T> {
    private Integer code;

    private String message;

    private T data;

    private ResultResponse(){};

    public ResultResponse(int code, T data) {
        this.code = code;
        this.data = data;
    }

    public ResultResponse(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public ResultResponse( String message, Integer code,T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }


    public static <T> ResultResponse<T> createdBySuccess(T data){
        return new ResultResponse<T>(ResultCode.SUCCESS.getCode(), data);
    }

    public static <T> ResultResponse<T> createBySuccessMessage(String message,T data) {
        return new ResultResponse<T>(ResultCode.SUCCESS.getCode(), message, data);
    }

    public static <T> ResultResponse<T> createBySuccessMessage(String message) {
        return new ResultResponse<T>(ResultCode.SUCCESS.getCode(), message);
    }

    public static <T> ResultResponse<T> createBySuccessMessage(String message,Integer code,T data) {
        return new ResultResponse<T>(message, code, data);
    }

    public static <T> ResultResponse<T> createByErrorMessage(String message,Integer code,T data) {
        return new ResultResponse<T>(message, code, data);
    }

    public static <T> ResultResponse<T> createByError(T data){
        return new ResultResponse<T>(ResultCode.ERROR.getCode(), ResultCode.ERROR.getDescription(), data);
    }

    public static <T> ResultResponse<T> createByErrorMessage(String errorMessage){
        return new ResultResponse<T>(ResultCode.ERROR.getCode(), errorMessage);
    }
}
