package com.explore.demo.mapper;

import com.explore.demo.pojo.TblStock;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author scy
 * @since 2022-05-31
 */
@Mapper
public interface TblStockMapper extends BaseMapper<TblStock> {

}
